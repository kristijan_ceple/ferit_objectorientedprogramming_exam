#include "J_CLL.h"
#include <memory>
#include <stdexcept>
#include "Node.h"
using namespace std;

J_CLL::J_CLL(int n, int k) : _n{ n }, _k{ k }
{
	// Need to instantiate the list here
	_root = make_shared<Node<int>>(1);
	
	// Need to instantiate n-1 others. Will handle the last one for the cyclic effect
	shared_ptr<Node<int>> prevNode = _root;
	for (int i = 2; i <= n; i++) {
		shared_ptr<Node<int>> newNode = make_shared<Node<int>>(i, prevNode);
		prevNode->setNext(newNode);
		prevNode = newNode;
	}

	// Need to connect root and last
	_root->setPrev(prevNode);
	prevNode->setNext(_root);

	// Should be it!
}

int J_CLL::operator[](int index)
{
	// for example if n is 12
	// if [-6, -1] we wanna go backwards, otherwise wanna go forward
	if (index < 0 || index >= _n) {
		throw std::out_of_range("Index out of range!");
	}

	shared_ptr<Node<int>> iter = _root;
	if (index >= (double)_n/2) {
		// Go Backwards
		for (int i = 0; i < _n - index; i++) {
			iter = iter->getPrev();
		}
	}
	else {
		// Go forwards
		for (int i = 0; i < index; i++) {
			iter = iter->getNext();
		}
	}
	
	return iter->getVal();
}

int J_CLL::survivor()
{
	shared_ptr<Node<int>> curr = _root;
	shared_ptr<Node<int>> prev = _root->getPrev();

	// Now go to the left with a counter, until only one if left
	int counter = 1;
	while (true)
	{
		// Check if only 1 is left
		if (curr == curr->getNext() && curr == curr->getPrev()) {
			return curr->getVal();
		}

		if (counter % _k == 0) {
			// Erase this node
			prev->setNext(curr->getNext());
			curr = curr->getNext();
			curr->setPrev(prev);
			counter++;
			continue;
		}

		counter++;
		prev = curr;
		curr = curr->getNext();
	}
}