#include <iostream>
#include <vector>
#include "Point3D.h"
#include "J_CLL.h"
#include <array>
#include <chrono>
#include <random>
#include <numeric>
#include <unordered_map>
#include <algorithm>
#include <string>

using namespace std;

struct Point {
	double x, y;

	double distance(const Point& p) const
	{
		return sqrt(x*x + y*y);
	}

	Point& operator+=(const Point& p)
	{
		x += p.x;
		y += p.y;
		return *this;
	}

	Point& operator/=(const double val)
	{
		x /= val;
		y /= val;
		return *this;
	}
};

vector<Point> kMeans(const vector<Point>& samples, unsigned int K, unsigned int T)
{
	// Will shuffle the elements and take the first K points as starting centres - representatives
	// Around them then the clustering will begin to be done
	vector<Point> representatives;
	vector<Point> toShuffle(samples);

	// ����������������������������������������������������������������
	// Using shuffle!
	// ����������������������������������������������������������������

	//long long seed = std::chrono::system_clock::now().time_since_epoch().count();
	//std::shuffle(toShuffle.begin(), toShuffle.end(), std::default_random_engine(seed));
	//for (unsigned int i = 0; i < K; i++) {
	//	Point toAdd = toShuffle[i];
	//	representatives.push_back(toAdd);
	//}

	// ����������������������������������������������������������������
	// Using std::sample C++17!
	// ����������������������������������������������������������������
	std::sample(samples.begin(), samples.end(),
		std::back_inserter(representatives), K,
		std::mt19937_64{ std::random_device{}() });


	// Have got the centres, time to cluster them together
	/*
	Need to constantly update the representatives. For each point need to calculate the nearest representative.
	First calculate the nearest representative, and update the clusters. Then update the representatives.
	*/
	vector<vector<Point>> clusters(K);
	for (unsigned int i = 0; i < T; i++) {
		// Calculate the nearest reprs for every point
		for (Point currPoint : samples) {
			// Need to find the nearest Repr
			double nearest = currPoint.distance(representatives[0]);
			size_t nearestIndex = 0;
			for (unsigned int j = 1; j < K; j++) {
				double newDist = currPoint.distance(representatives[j]);
				
				if (newDist < nearest) {
					nearest = newDist;
					nearestIndex = j;
				}
			}

			// Need to update the clusters now
			clusters[nearestIndex].push_back(currPoint);
		}

		// Update the representatives
		for (unsigned int j = 0; j < K; j++) {
			vector<Point> cluster = clusters[j];

			if (cluster.empty()) {
				continue;
			}

			Point repr = cluster[0];
			unsigned int clusterN = cluster.size();
			for (unsigned int z = 0; z < clusterN; z++) {
				repr += cluster[z];
			}
			repr /= clusterN;

			representatives[j] = repr;
		}

		// Need to clear the clusters now, as they shall be computed a new in the next iteration
		std::for_each(clusters.begin(), clusters.end(), [](vector<Point>cluster) { cluster.clear(); });
		//clusters.clear();
	}

	return representatives;
}


int main(void)
{
	Point3D<int> p1(0, 0, 0);
	Point3D<double> p2(-1.333, 2.456, 3);

	printf("%d %d %d\n", p1[0], p1[1], p1[2]);
	printf("%d %d %d\n", p1.x, p1.y, p1.z);
	printf("%f %f %f\n", p2[0], p2[1], p2[2]);
	printf("%f %f %f\n", p2.x, p2.y, p2.z);

	J_CLL testList(1, 2);
	cout << testList[0] << endl;
	
	J_CLL testList8(8, 3);
	cout << testList8[0] << " " << testList8[1] << " " << testList8[2] << " " << testList8[3] << " "
		<< testList8[4] << " " << testList8[5] << " " << testList8[6] << " " << testList8[7] << endl;

	cout << testList8.survivor() << endl;

	// Let's test the list a bit more
	J_CLL testList13(15, 3);
	cout << testList13.survivor() << endl;

	J_CLL testList12(12, 3);
	cout << testList12.survivor() << endl;

	J_CLL testList4(4, 2);
	cout << testList4.survivor() << endl;

	Point tp1{ 2, 4 };
	Point tp2{ 3, 1 };
	cout << (tp1 += tp2).x << " " << tp1.x << tp1.y << endl;

	vector<Point> points1 = { {1, 2}, {2, 3}, {1, 3}, 
							{20, 21}, {21, 22}, {19, 23}, 
							{101, 110}, {105, 120}, {125, 130},
							{505, 510}, {645, 555}, {600, 601}
	};

	vector<Point> points2 = { {1, 2}, {2, 3}, {1, 3},
							{20, 21}, {21, 22}, {19, 23},
							{40, 41}, {42, 43}, {41, 43}
	};

	vector<Point> reprs = kMeans(points1, 4, 1000);
	for (Point currPoint : reprs) {
		printf("{ %f, %f }\n", currPoint.x, currPoint.y);
	}
	cout << endl;

	reprs = kMeans(points2, 3, 1000);
	for (Point currPoint : reprs) {
		printf("{ %f, %f }\n", currPoint.x, currPoint.y);
	}

	return 0;
}