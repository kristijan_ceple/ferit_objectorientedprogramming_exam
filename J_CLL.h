#include "Node.h"
#include <memory>
using namespace std;

#pragma once
class J_CLL
{
private:
	int _n, _k;
	shared_ptr<Node<int>> _root;
public:
	J_CLL(int n, int k);
	int operator[](int index);
	int survivor();
};

