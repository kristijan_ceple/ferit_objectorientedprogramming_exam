#include <memory>
using namespace std;

#pragma once
template<typename T>
class Node
{
private:
	T _val;
	shared_ptr<Node<T>> _next, _prev;

	void init(T value, const shared_ptr<Node<T>>& prev, const shared_ptr<Node<T>>& next)
	{
		_val = value;
		_prev = prev;
		_next = next;
	}
public:
	Node(T value) : Node(value, nullptr, nullptr) {}

	Node(T value, const shared_ptr<Node<T>>& prev, const shared_ptr<Node<T>>& next)
	{
		init(value, prev, next);
	}

	Node(T value, const shared_ptr<Node<T>>& toPut, bool goPrev = true)
	{
		if (goPrev) {
			init(value, toPut, nullptr);
		}
		else {
			init(value, nullptr, toPut);
		}
	}

	T getVal()
	{
		return this->_val;
	}

	shared_ptr<Node<T>> getNext()
	{
		return this->_next;
	}

	shared_ptr<Node<T>> getPrev()
	{
		return this->_prev;
	}

	//void setNext(const Node<T>& toSet)
	//{
	//	_next = make_shared<Node<T>>(toSet);
	//}

	//void setPrev(const Node<T>& toSet)
	//{
	//	_prev = make_shared<Node<T>>(toSet);
	//}

	void setNext(const shared_ptr<Node<T>>& toSet)
	{
		_next = toSet;
	}

	void setPrev(const shared_ptr<Node<T>>& toSet)
	{
		_prev = toSet;
	}
};

