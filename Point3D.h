#pragma once
template<typename T>
class Point3D
{
private:
	T _data[3];
public:
	T x, y, z;

	Point3D(T x, T y, T z) : x{ x }, y{ y }, z{ z }
	{
		_data[0] = x;
		_data[1] = y;
		_data[2] = z;
	}

	T& operator[](int index)
	{
		if (index < 0 || index > 2) {
			throw std::out_of_range("Index needs to be in range [0,2]!");
		}

		return _data[index];
	}
};

